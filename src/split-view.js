(function() {
  var doc        = document,
      body       = doc.body,
      bodyStyle  = body.style,
      getStyles  = getComputedStyle,
      toInt      = parseInt,
      dragEvents = 'mousemove touchmove',
      stopEvents = 'mouseup touchend',
      startPosition, divider, view1, view2, horizontal, bodyResizeAttribute

  bindOrUnbind(doc, 'mousedown touchstart', start)

  function start(e) {
    divider = e.target.nodeName.toLowerCase() == 'split-divider' && e.target

    if (divider) {
      view1 = viewMeta(divider.previousElementSibling),   // get metadata of the view before divider
      view2 = viewMeta(divider.nextElementSibling, true)  // and after divider (true means after)

      startPosition = eventXY(e)
      horizontal = divider.parentNode.hasAttribute('horizontal')

      bodyResizeAttribute = 'split-view-' + (horizontal ? 'row-resize' : 'col-resize')
      body.setAttribute(bodyResizeAttribute, '')

      bindOrUnbind(doc, dragEvents, drag)
      bindOrUnbind(doc, stopEvents, stop)

      dispatch('split-view:start', e)

      return noop(e)
    }
  }

  function drag(e) {
    var MAX       = 'Max',
        MIN       = 'Min',
        PX        = 'px',
        position  = eventXY(e),
        delta     = horizontal ? (position.clientY - startPosition.clientY) : (position.clientX - startPosition.clientX),
        dimension = horizontal ? 'height' : 'width',
        dimension1, dimension2, shouldUpdate

    dimension1 = view1[dimension] + (view1.isAfter ? -delta : delta)  // new width or height of view1
    dimension2 = view2[dimension] + (view2.isAfter ? -delta : delta)  // new width or height of view2

    // shouldUpdate = dimensionMin <= dimension <= dimensionMax on both views
    shouldUpdate = view1[dimension + MIN] <= dimension1 && dimension1 <= view1[dimension + MAX] &&
                   view2[dimension + MIN] <= dimension2 && dimension2 <= view2[dimension + MAX]

    if (shouldUpdate) {
      if (!view1.filled) {
        view1.style[dimension] = dimension1 + PX
      }

      if (!view2.filled) {
        view2.style[dimension] = dimension2 + PX
      }
    }

    dispatch('split-view:drag', e)

    return noop(e)
  }

  function stop(e) {
    bindOrUnbind(doc, dragEvents, drag, true)
    bindOrUnbind(doc, stopEvents, stop, true)
    dispatch('split-view:stop', e)

    body.removeAttribute(bodyResizeAttribute)

    view1 = view2 = divider = startPosition = horizontal = bodyResizeAttribute = null

    return noop(e)
  }

  function viewMeta(dom, isAfter) {
    var MAX   = 9e+9,
        style = getStyles(dom)

    return {
      dom      : dom,
      style    : dom.style,
      filled   : dom.hasAttribute('fill'),
      width    : toInt(style.width),
      height   : toInt(style.height),
      widthMin : toInt(style['min-width'])  || 0,
      widthMax : toInt(style['max-width'])  || MAX,
      heightMin: toInt(style['min-height']) || 0,
      heightMax: toInt(style['max-height']) || MAX,
      isAfter  : isAfter
    }
  }

  function noop(e) {
    e.stopPropagation()
    e.preventDefault()
    return false
  }

  function bindOrUnbind(dom, eventNames, callback, shouldUnbind) {
    var fn = (shouldUnbind ? dom.removeEventListener : dom.addEventListener).bind(dom)

    eventNames.split(/\s+/).forEach(function(eventName) {
      fn(eventName, callback, true)
    })
  }

  function eventXY(e) {
    return (typeof e.clientX === 'number' && e) || (e.touches && e.touches[0])
  }

  function dispatch(eventName, e) {
    doc.dispatchEvent(new CustomEvent(eventName, {
      detail: {
        event  : e,
        view1  : view1.dom,
        divider: divider,
        view2  : view2.dom,
      }
    }))
  }
})();