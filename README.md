# split-view

A drop-in framework to make resizable split views.

## Install

`npm install split-view`

## Usage

### CSS

`@import 'split-view'`

### JavaScript

`import 'split-view'`

### HTML

View container:

`<split-view horizontal|verticle></split-view>`

Divider

`<split-divider wide|thin/>`

Add `fill` attribute to a view to make it stretched. Normally, you have views that have fixed width/height, the one with `fill` attribute will fill the rest of the container.

## Customize divider

```css
split-divider[thin],
split-divider[wide] {
  background-color: #333;
}

split-view[vertical] > split-divider[wide] {
  width: 20px;
}

split-view[horizontal] > split-divider[wide] {
  height: 20px;
}
```

## Sample

```html
<split-view horizontal>
    <header>Codepen</header>

    <split-view vertical fill>
      <section class="editor html"></section>

      <split-divider wide></split-divider>

      <section class="editor css"></section>

      <split-divider wide></split-divider>

      <section class="editor js" fill></section>
    </split-view>

    <split-divider wide></split-divider>

    <section class="preview"></section>
    <footer></footer>
</split-view>
```

See `samples` folder for more.